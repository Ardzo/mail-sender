# Library for work with e-mail

## Introduction
This is PHP class, which contains full set of functions for sending an e-mail.

## Requirements
* PHP 5.4.43 (maybe works with earlier versions).
* Windows or Unix.

## Using
Copy `"MailSender.php"` onto server and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.
