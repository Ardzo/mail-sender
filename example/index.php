<?php
namespace Ardzo\MailSender;

require 'MailSender.php';
// or if used a composer:
//require '/vendor/autoload.php';

$recipient_addr = 'example@example.com';
// Or like this:
$recipient_addr = array('example1@example.com','example2@example.com');

$mail = new MailSender;
$mail->attachFile('some_file.zip', 'any_name_of_some_file.zip', 'base64');
$mail->attachFile('some_file2.zip', 'any_name_of_some_file2.zip', 'base64');
$mail->from_addr = 'Sender\'s e-mail';
$mail->from_name = 'Sender\'s name';
$mail->to = $recipient_addr;
$mail->subj = 'Letter subject';
$mail->body = 'Test message';
$mail->priority = 3;
$mail->prepareLetter();
$mail->sendLetter();
?>
