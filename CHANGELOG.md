# Version history
## **1.4.3** 2018-10-08
* Aligned to PSR. Be careful: all functions renamed
* Example code is corrected
## **1.4.2** 2016-11-25
* Really removed "\n": in V1.4.1 not removed, only declared
## **1.4.1** 2016-10-10
* Adapted for work with PHP V5.4.43 (second "\n" is removed after "Content-Transfer-Encoding: base64")
## **1.4** 2008-11-25
* Now works fully on UTF-8
## **1.3.2** 2007-05-18
* Added: safe process if `$from_addr` is an array
## **1.3.1** 2007-02-06
* Safe call of `"mail()"` in `"send_letter()"`
## **1.3** 2006-08-08
* Ability to insert a picture just in body of letter is added
* Newlines a changed from "\r\n" to "\n" for correct showing of attachments
* Function `"attach_file()"` is chacnged: it is possible to specify name of file of attachment in letter
* In multipart-letters "text/plain", "text/html" encoded in KOI-8 but no in base64
## **1.2** 2004-03-25
* Var `"to"` in class can be array (multiple addresses)
## **1.1** 2004-03-22
* Headers in letter separates by "\n" instead of "\r\n" now - UNIX-format
## **1.0** 2003-11-23
* This is first release
